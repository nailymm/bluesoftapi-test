/**
 * 
 */
package com.bank.enums;

/**
 * @author naily
 *
 */
public enum TransactionStatus {
	SUCCESS,
    FAILURE_SAME_ACCOUNT,
    FAILURE_ACCOUNT_NOT_FOUND,
    FAILURE_INSUFFICIENT_BALANCE
}
