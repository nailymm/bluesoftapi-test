/**
 * 
 */
package com.bank.controller;

import java.util.Collection;

import javax.persistence.NoResultException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.bank.entity.Account;
import com.bank.service.AccountService;

/**
 * @author naily
 *
 */
@RestController
@RequestMapping("/api/account")
public class AccountController extends BaseController{

    @Autowired
    private AccountService accountService;

    /**
     * Endpoint para obtener lista de cuentas registradas
     * Si las encuentra retorna un HTTP status 200.
     * Si no encuentra resultado retorna 404
     *
     * @param
     * @return Un ResponseEntity con la lista de cuentas en formato json.
     */
    @RequestMapping(value= "/allAccounts",method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Collection<Account>> getAllAccounts(){
        Collection<Account> accounts = accountService.findAll();
        if (accounts.isEmpty()){
            throw new NoResultException("Not accounts found");
        }
        return new ResponseEntity<>(accounts, HttpStatus.OK);
    }
    

    /**
     * Endpoint para registrar una cuenta
     * Si la registra con exito retorna un HTTP status 201.
     * Si ocurre algun error retorna 500
     *
     * @param account en formato json, que contenga el banco, el pais, y el saldo
     *                inicial de la cuenta
     * @return Un ResponseEntity con el detalle de la cuenta registrada en formato json.
     */
    @RequestMapping(value= "/insert", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Account> insert(@RequestBody Account account){
        Account ac = accountService.create(account);
        if (ac == null){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(ac, HttpStatus.CREATED);
    }

    /**
     * Endpoint para editar una cuenta
     * Si la edita con exito retorna un HTTP status 200.
     * Si la cuenta no existe retorna 404
     * Si ocurre algun error retorna 500
     *
     * @param account en formato json, que contenga el banco, el pais, y el saldo
     *                de la cuenta
     * @return Un ResponseEntity con el detalle de la cuenta modificada en formato json.
     */
    @RequestMapping(method = RequestMethod.PUT, value= "/{id}/update",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Account> update(@RequestBody Account account){
        Account a = accountService.findOne(account.getId());
        if (a == null){
            throw new NoResultException("Account " + account.getId() + " not found");
        }
        Account ac = accountService.update(account);
        if (ac == null){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(ac, HttpStatus.OK);
    }

    /**
     * Endpoint para eliminar una cuenta
     * Si la elimina con exito retorna un HTTP status 204.
     * Si la cuenta no existe retorna 404
     *
     * @param id en de la cuenta que desea eliminar
     * @return
     */
    @RequestMapping(value="/{id}/delete", method = RequestMethod.DELETE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Account> delete(@PathVariable("id") long id){
        Account a = accountService.findOne(id);
        if (a == null){
            throw new NoResultException("Account " + id + " not found");
        }
        accountService.delete(id);
        return new ResponseEntity<>(null, null, HttpStatus.NO_CONTENT);
    }

}
