/**
 * 
 */
package com.bank.enums;

/**
 * @author naily
 *
 */
public enum TransactionType {
	DEPOSIT, WITHDRAW, TRANSFERENCE
}
