## Bluebank API
API REST para procesar transacciones bancarias usando Spring Boot, con persistencia en memoria con JPA y Hibernate usando base de datos H2.

## Especificaciones tecnicas
Java 11
Spring boot 2.5.2
Maven 4.0
JPA y Hibernate
Log4j 2

## Setup:
Clonar repositorio.
`$ git clone https://gitlab.com/nailymm/bluesoftapi-test.git`

Hacer el build con maven.
`$ mvn clean package`

Ejecutar con el tomcat embebido de spring boot.
`$ mvn spring-boot:run`

La aplicacion queda disponible en: http://localhost:8080

Para las configuraciones pertinentes, dirigirse al application.properties

## Mejoras Futuras
- Aplicación de Spring Security para el logging.
- Aplicación de Tests Unitarios
- Revisión de bitacora para transacciones
- Revisión de Entidad Cliente para ampliar funcionalidad.
