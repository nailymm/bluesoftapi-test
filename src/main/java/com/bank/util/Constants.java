/**
 * 
 */
package com.bank.util;

/**
 * @author naily
 *
 */
public class Constants {
	public static double INTERNAL_TAX = 0.03;
    public static double EXTERNAL_TAX = 0.05;
}
