/**
 * 
 */
package com.bank.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bank.entity.Account;
import com.bank.repository.AccountRepository;

/**
 * @author naily
 *
 */
@Service
public class AccountService {

	@Autowired
    private AccountRepository accountRepository;

    public Collection<Account> findAll(){
        return accountRepository.findAll();
    }

    public Account findOne(long id){
        return accountRepository.getById(id);
    }

    public Account create(Account account){
        return accountRepository.save(account);
    }

    public Account update(Account account){
        return accountRepository.save(account);
    }
    public Account findByAccountNumber(String accountNumber) {
    	return accountRepository.findByAccountNumber(accountNumber);
    }
    public Account delete(long id){
        Account account = accountRepository.getById(id);
        accountRepository.delete(account);
        return account;
    }
}
