/**
 * 
 */
package com.bank.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bank.entity.Account;
import com.bank.entity.Transaction;
import com.bank.enums.TransactionStatus;
import com.bank.enums.TransactionType;
import com.bank.repository.AccountRepository;
import com.bank.repository.TransactionRepository;
import com.bank.util.TransactionLogger;

import static com.bank.util.Constants.*;
/**
 * @author naily
 *
 */
@Service
public class TransactionService {
	@Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private AccountRepository accountRepository;

    public Collection<Transaction> findAll(){
        return transactionRepository.findAll();
    }

    public Transaction find(long id){
        return transactionRepository.getById(id);
    }

    /**
     * Procesamiento de la transaccion
     *
     * Se calcula el tipo, el impuesto y el estado de la misma
     * Y se loguea en un archivo de texto con las transacciones
     * (independientemente del estado)
     *
     * @param transaction
     * @return transaction
     */
    public Transaction process(Transaction transaction) {

        // Seteo el tipo de transaccion
        TransactionType type = transaction.getType();
        transaction.setType(type);

        // Calculo el impuesto
        double tax = computeTax(transaction);
        transaction.setTax(tax);

        // Chequeo si se puede aprobar la transaccion
        TransactionStatus status = computeStatus(transaction);
        transaction.setStatus(status);

        if (status == TransactionStatus.SUCCESS){
            save(transaction);
        }
        TransactionLogger.log(transaction);
        return transaction;
    }

    /**
     * Se guarda y loguea de la transaccion,
     * Una vez que se hicieron las validaciones se procesa la transaccon
     * Se actualizan los saldos de las cuentas y se guarda la transaccion
     * @param transaction
     * @return transaction Reterorna la transaccion modificada
     */
    public Transaction save(Transaction transaction){
        Account source = accountRepository.findByAccountNumber(transaction.getSource());
        Account target = accountRepository.findByAccountNumber(transaction.getTarget());

        double totalAmount = transaction.getAmount() + transaction.getTax();
        double currentSourceBalance = source.getBalance();
        double currentTargetBalance = target.getBalance();

        // Actualizo los salgos de las cuentas de origen y destino
        source.setBalance(currentSourceBalance - totalAmount);
        target.setBalance(currentTargetBalance + totalAmount);
        accountRepository.save(source);
        accountRepository.save(target);

        return transactionRepository.save(transaction);
    }
    
    public Transaction withdraw(Transaction transaction){
    	Account account = accountRepository.findByAccountNumber(transaction.getSource());
    	double totalAmount = 0;
    	if (transaction.getSource().equalsIgnoreCase(transaction.getTarget())) {
    		totalAmount = transaction.getAmount() + transaction.getTax();
    	}

        // Actualizo los salgos de las cuentas de origen y destino
        account.setBalance(account.getBalance() - totalAmount);
        accountRepository.save(account);

        return transactionRepository.save(transaction);
    }
        
    /**
     * Setea los diferentes estados posibles
     *
     * SUCCESS: se aprueba la transaccion
     * FAILURE_SAME_ACCOUNT: error porque la cuenta de origen y destino es la misma
     * FAILURE_ACCOUNT_NOT_FOUND: alguna de las cuentas no existe
     * FAILURE_INSUFFICIENT_BALANCE: la cuenta de origen no tiene saldo suficiente
     *
     * @param transaction
     * @return TransactionStatus
     */
    public TransactionStatus computeStatus(Transaction transaction) {
        // Denegado si los numero de cuenta son iguales
        if (transaction.getSource() == transaction.getTarget())
            return TransactionStatus.FAILURE_SAME_ACCOUNT;

        // Denegado si no existe la cuenta de origen
        Account source = accountRepository.findByAccountNumber(transaction.getSource());
        if (source == null)
            return TransactionStatus.FAILURE_ACCOUNT_NOT_FOUND;

        // Denegado si no existe la cuenta de destino
        Account target = accountRepository.findByAccountNumber(transaction.getTarget());
        if (target == null)
            return TransactionStatus.FAILURE_ACCOUNT_NOT_FOUND;

        // Denegado si no tiene fondos la cuenta de origen
        double resultSourceBalance = source.getBalance() - transaction.getAmount() - transaction.getTax();
        if (resultSourceBalance < 0)
           return TransactionStatus.FAILURE_INSUFFICIENT_BALANCE;

        // Se aprueba la transaccion si todo salio bien
        return TransactionStatus.SUCCESS;
    }

    /**
     * Calcula el impuesto dependiendo del tipo de transaccion
     *
     * @param transaction
     * @return double tax con monto del impuesto calculado
     */
    public double computeTax(Transaction transaction) {
        // Calculo del impuesto dependiendo del tipo de transaccion
        if (transaction.getType().equals(TransactionType.WITHDRAW) ) {
            return transaction.getAmount() * INTERNAL_TAX;
        } else if (transaction.getType().equals(TransactionType.TRANSFERENCE) ){
            return transaction.getAmount() * EXTERNAL_TAX;
        }
        return 0.00;
    }


}
