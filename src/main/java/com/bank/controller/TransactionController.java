/**
 * 
 */
package com.bank.controller;

import java.util.Collection;

import javax.persistence.NoResultException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bank.entity.Transaction;
import com.bank.enums.TransactionStatus;
import com.bank.service.TransactionService;


/**
 * @author naily
 *
 */
@RestController
@RequestMapping("/api/transaction")
public class TransactionController extends BaseController{
	@Autowired
    private TransactionService transactionService;

    /**
     * Endpoint para obtener una lista de las transacciones
     * Si las encuentra retorna un HTTP status 200
     * @param
     * @return Un ResponseEntity con la lista de transacciones en formato Json.
     */
    @RequestMapping(value="/all", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Collection<Transaction>> findAll(){
        Collection<Transaction> t = transactionService.findAll();
        if (t.isEmpty()){
            throw new NoResultException("Not transaction found");
        }
        return new ResponseEntity<>(t, HttpStatus.OK);
    }
    /**
     * Endpoint para obtener detalle de una transaccion por id
     *
     * Si las encuentra retorna un HTTP status 200.
     *
     * Si no encuentra resultado retorna 404
     *
     * @param id de la transaccion a consultar
     * @return Un ResponseEntity con la lista de transacciones en formato Json.
     */
    @RequestMapping(value="/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Transaction> find(@PathVariable("id") long id){
        Transaction t = transactionService.find(id);
        if (t == null){
            throw new NoResultException("Transaction " + id + " not found");
        }
        return new ResponseEntity<>(t, HttpStatus.OK);
    }

    /**
     * Endpoint para procesar una transaccion
     * Si la puede registrar retorna un HTTP status 201.
     * Si no encuentra alguna de las cuentas de origen o destino retorna 404
     * Si intenta transferir a la misma cuenta o la cuenta de origen
     * tiene saldo insuficiente retorna 422
     * Si ocurre un error retorna HTTP status 500.
     *
     * @param transaction en formato json, que contenga la cuenta de origen, destino
     *                    y el monto a transferir
     * @return Un ResponseEntity con el detalle de la transaccion procesada, en el
     * campo 'status' se observa el estado de la transaccion, 'SUCCESS' indica que
     * se hizo la transferencia con exito, cualquiero otro estado indica que hubo
     * error
     */
    @RequestMapping(value="/insert", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Transaction> insert(@RequestBody Transaction transaction){
    	Transaction transactionResp = transactionService.process(transaction);
        if (transactionResp == null){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if (transaction.getStatus() == TransactionStatus.FAILURE_ACCOUNT_NOT_FOUND){
        	return new ResponseEntity<>(transactionResp, HttpStatus.NOT_FOUND);
        } else if (transactionResp.getStatus() == TransactionStatus.FAILURE_SAME_ACCOUNT){
        	return new ResponseEntity<>(transactionResp, HttpStatus.UNPROCESSABLE_ENTITY);
        } else if (transactionResp.getStatus() == TransactionStatus.FAILURE_INSUFFICIENT_BALANCE){
        	return new ResponseEntity<>(transactionResp, HttpStatus.UNPROCESSABLE_ENTITY);
        } else {
        	return new ResponseEntity<>(transactionResp, HttpStatus.CREATED);
        }
    }
    
    @RequestMapping(value="/withdraw", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Transaction> withdraw(@RequestBody Transaction transaction){
    	Transaction transactionResp = transactionService.withdraw(transaction);
        if (transactionResp == null){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if (transaction.getStatus() == TransactionStatus.FAILURE_ACCOUNT_NOT_FOUND){
        	return new ResponseEntity<>(transactionResp, HttpStatus.NOT_FOUND);
        } else if (transactionResp.getStatus() == TransactionStatus.FAILURE_SAME_ACCOUNT){
        	return new ResponseEntity<>(transactionResp, HttpStatus.UNPROCESSABLE_ENTITY);
        } else if (transactionResp.getStatus() == TransactionStatus.FAILURE_INSUFFICIENT_BALANCE){
        	return new ResponseEntity<>(transactionResp, HttpStatus.UNPROCESSABLE_ENTITY);
        } else {
        	return new ResponseEntity<>(transactionResp, HttpStatus.CREATED);
        }
    }

}
